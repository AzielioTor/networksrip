package rip;

import java.net.SocketException;
import java.util.Iterator;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * This thread continually checks how long its been since the router has 
 * heard from its neighbors and makes them infinity if past the threshold.
 * @author Aziel Shaw
 * @version October 23, 2018
 */
public class TableTimerChecker extends Thread {
    // Leeprunning thread
    public boolean keeprunning = true;
    // Owner variable
    private Router router;
    // Random variable for random time and variables for time
    private Random random;
    private final int INIT_SLEEP = 15000; // 15 seconds
    private final int RAND_BOUND = 701;
    private final int CONST_BOUND = 3000; // 3 seconds
    // Threshold for making nodes infinity
    public static final int INFINITY_THRESHOLD = 9000; // 9 seconds
    // Threshold for removing nodes
    private final int GARBAGE_THRESHOLD = 3 * INFINITY_THRESHOLD; // 3 times the Threshold
    // Number to call "infinity"
    public static final int INFINITY = 16;
    
    /**
     * Constructor for the table timer checker thread
     * @param r The Router this thread belongs to.
     */
    public TableTimerChecker(Router r) {
        this.router = r;
        this.random = new Random();
    }
    
    /**
     * Kills the thread
     */
    public void killThread() {
        this.keeprunning = false;
    }
    
    /**
     * Runs the thread
     */
    @Override
    public void run() {
        int randomtime = 0;
        try {
            // Sleep initially whie I set up the other routers.
            this.sleep(INIT_SLEEP);
        } catch (InterruptedException ex) {
            Logger.getLogger(TableTimerChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        while(keeprunning) {
            // Sleep Thread
            try {
                checkTableTimes();
                randomtime = random.nextInt(RAND_BOUND) - (RAND_BOUND / 2);
                Thread.sleep(CONST_BOUND + randomtime);
            } catch (InterruptedException ex) {
                Logger.getLogger(TableTimerChecker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * This thread checks the tables times and sleeps nodes if they are beyond
     * the threshold
     */
    public void checkTableTimes() {
        RoutingTable rt = this.router.getTable();
        Iterator<String> itr = rt.getNeighbors().iterator();
        String currIP = "";
        long currTime = System.currentTimeMillis();
        boolean tableChange = false;
        // Iterate over neighbors
        while(itr.hasNext()) {
            // Get next Nieghbor IP
            currIP = itr.next();
            // Get last recorded time
            long IPtime = rt.getTimes().get(currIP);
            // Check time difference
            long result = currTime - IPtime;
            if(result > INFINITY_THRESHOLD && rt.getDistances().get(currIP) < INFINITY) {
                System.out.println("ROUTER FAILED: " + currIP);
                sleepNode(rt, currIP);
                sleepNextHopNodes(rt, currIP);
                tableChange = true;
                // print update
                System.out.println(rt.toString());
            } else if(result > GARBAGE_THRESHOLD) { }
        }
        // If the table changed, send triggered update
        if(tableChange) {
            try {
                System.out.println("SENDING OUT TRIGGERED UPDATE DUE TO ROUTER FAILURE: " + currIP);
                // Send triggered update
                TableSenderThread tst = new TableSenderThread(this.router, false);
                tst.start();
            } catch (SocketException ex) {
                Logger.getLogger(TableTimerChecker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * Makes a nodes information infinity
     * @param rt The routing table to change information off of
     * @param currIP The IP to "sleep" in the routing table
     */
    private void sleepNode(RoutingTable rt, String currIP) {
        rt.getDistances().put(currIP, INFINITY);
        rt.getNextHops().put(currIP, "inf");
    }
    
    /**
     * Make a nodes next hops for an inf node, inf
     * @param rt The routing table to change information off of
     * @param currIP The IP to "sleep" in the routing table
     */
    private void sleepNextHopNodes(RoutingTable rt, String currIP) {
        Iterator<String> itr = rt.getIpsFromTable().iterator();
        while(itr.hasNext()) {
            String curr = itr.next();
            if(rt.getNextHops().get(curr).equals(currIP)) { 
                sleepNode(rt, curr);
            } // End If
        } // End While
    } // End Method
}
