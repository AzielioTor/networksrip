package rip;

import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main "router" object where everything interacts with. Has threads to handle interactions.
 * @author Aziel Shaw
 * @version October 23, 2018
 */ 
public class Router extends Thread {
    // Variables local to this class
    private final String thisIP;
    private final String subnet_mask;
    // Socket variables
    private DatagramSocket reg_socket;
    private final int SEND_REGULAR = 9898;
    // This classes Routing Table
    private RoutingTable table;
    // Threads for sending, recieving, and triggered events
    private PacketRecieverThread reg_mrt;
    private TableSenderThread tst;
    // Other helper threads
    private TableTimerChecker ttct;
    private TablePrinterThread tpt;
    
    /**
     * Construct Router
     * @param IP The IP of THIS router
     * @param subnet_mask The Subnet Mast of THIS router
     * @param ips The initial neighboring IPs
     * @param subnets The initial subnets of the neighboring IPs
     * @param distances The initial distances of the neighboring IPs
     * @throws UnknownHostException Throws an UnknownHostException if the host
     *             is unknown
     * @throws SocketException Throw a SocketException if an error happens
     *             regarding one of the sockets
     */
    public Router(String IP, 
            String subnet_mask, 
            ArrayList<String> ips, 
            HashMap<String, String> subnets, 
            HashMap<String, Integer> distances) 
            throws UnknownHostException, SocketException {
        // Store initial variables for this class
        this.thisIP = IP;
        this.subnet_mask = subnet_mask;
        // Create and initialize Routing Table
        table = new RoutingTable(getIP(), getSubnetMask(), ips, subnets, distances);
        // Create Sockets
        reg_socket = new DatagramSocket(SEND_REGULAR);
        // Start threads
        try {
            reg_mrt = new PacketRecieverThread(this, reg_socket);
            tst = new TableSenderThread(this, true);
            tpt = new TablePrinterThread(this);
            ttct = new TableTimerChecker(this);
            startThreads();
        } catch (SocketException ex) {
            Logger.getLogger(Router.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Print initial routing Table
        printTable();
    }
    
    /**
     * Returns the IP of the router
     * @return The routers IP
     */
    public String getIP() {
        return this.thisIP;
    }
    
    /**
     * Returns the SubnetMask of the router
     * @return The routers subnet mask
     */
    public String getSubnetMask() {
        return this.subnet_mask;
    }
    
    /**
     * Returns the routingtable of the router
     * @return The routers routing table
     */
    public RoutingTable getTable() {
        return this.table;
    }
    
    /**
     * Starts the Routers local threads
     */
    public void startThreads() {
        try {
            // Time checker
            ttct.start();
            this.sleep(70);
            // Packet Reciever
            reg_mrt.start();
            this.sleep(70);
            // Table Sender
            tst.start();
            this.sleep(70);
            // TablePrinter
            tpt.start();
        } catch (InterruptedException ex) {
            Logger.getLogger(Router.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Kill the routers recieving thread.
     * @throws InterruptedException Thrown if interruption occurs
     */
    public void endReciever() throws InterruptedException {
        reg_mrt.killThread();
        this.sleep(10);
    }
    
    /**
     * Kill the routers table sending thread.
     * @throws InterruptedException Thrown if interruption occurs
     */
    public void endSender() throws InterruptedException {
        tst.killThread();
        this.sleep(10);
    }
    
    /**
     * Kill the routers table updating thread.
     * @throws InterruptedException Thrown if interruption occurs
     */
    public void endTableUpdater() throws InterruptedException {
        ttct.killThread();
        this.sleep(10);
    }
    
    /**
     * Kill the routers table printing thread.
     * @throws InterruptedException Thrown if interruption occurs
     */
    public void endPrinter() throws InterruptedException {
        tpt.killThread();
        this.sleep(10);
    }
    
    
    /**
     * Main loop of this class. 
     */
    @Override
    public void run() {
        while (true) { }
    }
    /**
     * Print the routers table
     */
    public void printTable() {
        getTable().printTable();
    }
    
    /**
     * Print the routers neighbors
     */
    public void printNeighbors() {
        getTable().printNeighbors();
    }
    
    /**
     * Change a neighbors cost metric
     */
    public void changeNeighborCost() {
        System.out.println("TODO: changeNeighborCost");
    }
    
    /**
     * Sends the table to a node
     */
    public void sendTableToNode() {
        TableSenderThread stst;
        try {
            stst = new TableSenderThread(this, false);
            stst.start();
        } catch (SocketException ex) {
            System.err.println("Packet didn't send.");
            Logger.getLogger(Router.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    /**
     * Print the information of this node
     */
    public void printNodeInfo() {
        System.out.println("This IP address: " + getIP());
        System.out.println("This subnet mask: " + getSubnetMask());
    }

    /**
     * Closes Program
     */
    public void exit() {
        closeClient();
        System.out.println("Shutting Down...");
        System.exit(0);
    }
    
    /**
     * This method handles all the logic for shutting down the server
     */
    public void closeClient() {
        try {
            endPrinter();
            endTableUpdater();
            endSender();
            endReciever();
        } catch (InterruptedException ex) {
            Logger.getLogger(Router.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
