package rip;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Thread class which prints the table periodically
 * @author Aziel Shaw
 * @version October 23, 2018
 */
public class TablePrinterThread extends Thread {
    // Leeprunning thread
    public boolean keeprunning = true;
    private Router router;
    // Sleep time
    private final int SLEEP_TIME = 3000;
    
    /**
     * Constructor for the printing thread
     * @param r The owning router
     */
    public TablePrinterThread(Router r) {
        this.router = r;
        
    }
    
    /**
     * Stop this thread from running
     */
    public void killThread() {
        this.keeprunning = false;
    }
    
    /**
     * The main running method for this thread
     */
    @Override
    public void run() {
        while(keeprunning) {
            try {
                this.router.printTable();
                this.sleep(SLEEP_TIME);
            } catch (InterruptedException ex) {
                System.err.println("ERROR PRINTING TABLE IN TABLEPRINTERTHREAD");
                Logger.getLogger(TablePrinterThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
