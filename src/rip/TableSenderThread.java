package rip;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class continously sends the table information to the owners neighbors
 * @author Aziel Shaw
 * @version October 23, 2018
 */
public class TableSenderThread extends Thread {
    // Socket variables
    private DatagramSocket socket;
    private final int recieverport = 9898;
    // Buffer variables
    private byte[] buffer;
    // Reference to the owning Router Object
    private Router r;
    // Random variable for random time and variables for time
    private Random random;
    private final int RAND_BOUND = 701;
    private final int CONST_BOUND = 5000;
    // Leeprunning thread
    public boolean keeprunning;
    
    /**
     * Base constructor of the sender thread, stays running when called
     * @param r The owner router
     * @throws SocketException Throws a socket exception
     */
    public TableSenderThread(Router r) throws SocketException {
        this(r, true);
    }
    
    /**
     * Constructor of this thread
     * @param r The owner router
     * @param running If the thread will continuously run or just send once and die
     * @throws SocketException Throws a socket exception
     */
    public TableSenderThread(Router r, boolean running) throws SocketException {
        this.r = r;
        socket = new DatagramSocket();
        this.random = new Random();
        this.keeprunning = running;
    }
    
    /**
     * Stop this thread from running
     */
    public void killThread() {
        this.keeprunning = false;
    }
    
    /**
     * The main run method of this thread
     */
    @Override
    public void run() {
        int randomtime;
        while(keeprunning) {
            // Send table to neighbors
            ArrayList<String> neighbors = r.getTable().getNeighbors();
            Iterator<String> itr = neighbors.iterator();
            while(itr.hasNext()) {
                // Get current neighbor
                String destinationIP = itr.next();
                // Create Message/buffer
                buffer = createMessage(destinationIP);
                // Get address Set
                InetAddress address = null;
                try {
                    address = InetAddress.getByName(destinationIP);
                } catch (UnknownHostException ex) {
                    Logger.getLogger(TableSenderThread.class.getName()).log(Level.SEVERE, null, ex);
                }
                // Set up packet
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address, this.recieverport);
                // Send packet
                try {
                    socket.send(packet);
                } catch (IOException ex) {
                    Logger.getLogger(TableSenderThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            // Sleep Thread
            try {
                randomtime = random.nextInt(RAND_BOUND) - (RAND_BOUND / 2);
                this.sleep(CONST_BOUND + randomtime);
            } catch (InterruptedException ex) {
                Logger.getLogger(TableSenderThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * Creates a table specifically for the Destination IP
     * @param destinationIP The destination IP to send to
     * @return The table in byte[] form to send
     */
    private byte[] createMessage(String destinationIP) {
        byte[] new_buffer = new byte[1024];
        ByteArrayOutputStream byteOutStream = null;
        ObjectOutputStream outStream = null;
        try {
            byteOutStream = new ByteArrayOutputStream();
            outStream = new ObjectOutputStream(byteOutStream);
            RoutingTable tableCopy = new RoutingTable(r.getTable());
            // Modify the copied table
            Iterator<String> itr = tableCopy.getIpsFromTable().iterator();
            while(itr.hasNext()) {
                String str = itr.next();
                if(tableCopy.getNextHops().get(str).equals(destinationIP)) {
                    tableCopy.updateDistance(str, TableTimerChecker.INFINITY);
                }
            }
            // Write copied table to byte[]
            outStream.writeObject(tableCopy);
            outStream.flush();
            new_buffer = byteOutStream.toByteArray();
        } catch(IOException ioe) {
            System.err.println(ioe.toString());
            ioe.printStackTrace();
        } finally {
            try {
                if(outStream != null) {
                    outStream.close();
                }
                if(byteOutStream != null) {
                    byteOutStream.close();
                }
            } catch(IOException ioe) {
                System.err.println(ioe.toString());
                ioe.printStackTrace();
            }
        }
        return new_buffer;
    }
    
}
