package rip;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Main router launcher. Sets up initial values and runs the router.
 * @author Aziel Shaw
 * @version October 23, 2018
 */
public class RipMain {

    /**
     * Main method
     * @param args the command line arguments
     */
    public static void main(String[] args) throws UnknownHostException, UnsupportedEncodingException, SocketException {
        HashMap<String, String> knownhosts = getRITHosts();
        Scanner scan = new Scanner(System.in);
        // Get the IP address of the current router and print
        String thisIPAddr = InetAddress.getLocalHost().getHostAddress();
        System.out.println("\n\nStarting RIP: This IP Address is: " + thisIPAddr);
        System.out.println("Default subnet mast is 255.255.255.0");
        System.out.print("Please enter Subnet Mask of this node (enter d for default)\n>> ");
        String thisSNM = scan.nextLine();
        if(thisSNM.equalsIgnoreCase("d"))
            thisSNM = "255.255.255.0";
        // Ask user for num neighbors
        System.out.print("How many neighbors do you have?\n>> ");
        Integer j = scan.nextInt();
        // Variables for storing neighbors
        ArrayList<String> ips = new ArrayList<>(); // ips
        HashMap<String, String> subnets = new HashMap<>(); // subnets
        HashMap<String, Integer> distances = new HashMap<>(); // distances
        String currIP = "";
        String currSNM = "";
        // Iterate over num neighbors and store values
        for(int i = 1; i <= j; i++) {
            System.out.print("IP of neighbor " + i + "\n>> ");
            currIP = scan.next();
            if(knownhosts.containsKey(currIP)) {
                ips.add(knownhosts.get(currIP));
                currIP = knownhosts.get(currIP);
            }   
            else
                ips.add(currIP);
            System.out.print("Subnet Mask of neighbor " + i + " (enter d for default)\n>> ");
            currSNM = scan.next();
            if(currSNM.equalsIgnoreCase("d"))
                subnets.put(currIP, "255.255.255.0");
            else
                subnets.put(currIP, currSNM);
            System.out.print("Distance of neighbor " + i + "\n>> ");
            distances.put(currIP, scan.nextInt());
            System.out.println("");
        }
        System.out.println("Setup complete! Press enter when ready to start router(" + thisIPAddr + ")");
        scan.next();
        scan.close();
        // Create Router and add THIS routers data to the table
        ips.add(thisIPAddr);
        subnets.put(thisIPAddr, thisSNM);
        distances.put(thisIPAddr, 0);
        Router r = new Router(thisIPAddr, thisSNM, ips, subnets, distances);
        r.start();
    }
    
    /**
     * Returns a hashmap of KNOWN address for ease of startup
     * @return 
     */
    private static HashMap<String, String> getRITHosts() {
        HashMap<String, String> hosts = new HashMap<>();
        hosts.put("queeg", "129.21.30.37");
        hosts.put("rhea", "129.21.37.49");
        hosts.put("comet", "129.21.34.80");
        hosts.put("glados", "129.21.22.196");
        hosts.put("q", "129.21.30.37");
        hosts.put("r", "129.21.37.49");
        hosts.put("c", "129.21.34.80");
        hosts.put("g", "129.21.22.196");
        return hosts;
    }
    
}
