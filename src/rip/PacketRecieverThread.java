package rip;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class handles receiving packets for the router
 *
 * @author Aziel Shaw
 * @version October 23, 2018
 */
public class PacketRecieverThread extends Thread {

    // Socket stuff
    private DatagramSocket socket;
    // Buffer stuff
    private byte[] buffer;
    private final int BYTE_LENGTH = 1024;
    // Router class that owns this thread
    private Router router;
    // Know when to kill the thread
    private boolean keeprunning = true;

    /**
     * Constructor for the receiver thread.
     *
     * @param r The router which owns this thread
     * @param ds The DatagramSocket of which to receive packets from.
     * @throws SocketException If there's an exception within the Socket
     */
    public PacketRecieverThread(Router r, DatagramSocket ds) throws SocketException {
        this.router = r;
        socket = ds;
    }

    /**
     * Returns the router object associated with this thread
     *
     * @return The router
     */
    private Router getRouter() {
        return this.router;
    }

    /**
     * Kills the thread from running.
     */
    public void killThread() {
        this.keeprunning = false;
    }

    /**
     * Start the thread and run it
     */
    @Override
    public void run() {
        DatagramPacket packet;
        buffer = new byte[BYTE_LENGTH];
        RoutingTable newTable = null;
        ByteArrayInputStream byteInStream = null;
        ObjectInputStream inStream = null;
        while (keeprunning) {
            try {
                packet = new DatagramPacket(buffer, BYTE_LENGTH);
                socket.receive(packet);
                buffer = packet.getData();
            } catch (IOException iOException) {
                System.err.println("Something went wrong:");
                System.err.println("MessageRecieverThread run method");
                System.err.println(iOException.getMessage());
                System.err.println(iOException.getLocalizedMessage());
            }
            // Get Data from packet
            try {
                byteInStream = new ByteArrayInputStream(buffer);
                inStream = new ObjectInputStream(byteInStream);
                newTable = (RoutingTable) inStream.readObject();
            } catch (ClassNotFoundException cnfe) {
                Logger.getLogger(PacketRecieverThread.class.getName()).log(Level.SEVERE, null, cnfe);
            } catch (IOException ioe) {
                Logger.getLogger(PacketRecieverThread.class.getName()).log(Level.SEVERE, null, ioe);
            }
            // Update Routing Table
            boolean updated = false;
            RoutingTable this_rt = this.router.getTable();
            updated = checkTableUpdate(this_rt, newTable);
            // Send tables if there was a change.
            if (updated) {
                try {
                    TableSenderThread tst = new TableSenderThread(this.router, false);
                    tst.start();
                } catch (SocketException ex) {
                    Logger.getLogger(PacketRecieverThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Check if there is an update and show the change.
     *
     * @param thisTable The table this thread belongs to
     * @param neighborTable The table that we just received
     * @return Whether or not there's a change that needs to be update.
     */
    private boolean checkTableUpdate(RoutingTable thisTable, RoutingTable neighborTable) {
        // update time
        long currtime = System.currentTimeMillis();
        thisTable.getTimes().put(neighborTable.getOwner(), currtime);
        // update table
        boolean change = false;
        Iterator<String> itr = neighborTable.getIpsFromTable().iterator();
        if (neighborTable.getIpsFromTable().size() < 3) {
            itr = neighborTable.getNeighbors().iterator();
        }
        while (itr.hasNext()) {
            String currIP = itr.next();
            // If the IP we're adding is the current IP
            if (currIP.equals(thisTable.getOwner())) { }
            // If we hear from an IP that doesn't exist yet
            else if (!thisTable.getIpsFromTable().contains(currIP)) {
                boolean add = true;
                // currIP is current IP
                // get Subnet
                String subnet = neighborTable.getSubNets().get(currIP);
                // get Distance
                int dist = 0;
                try {
                    dist = neighborTable.getNeighborDistances().get(currIP);
                    dist += thisTable.getNeighborDistances().get(neighborTable.getOwner());
                } catch (NullPointerException npe) {
                    add = false;
                    break;
                }
                String distance = dist + "";
                // get nextHop
                String nexthop = neighborTable.getOwner();
                // Add entry to table
                if (add) {
                    thisTable.addEntry(currIP,
                            subnet,
                            distance,
                            nexthop);
                    change = true;
                }
                // If node is infinite BUT you've heard from it recently under the time threshhold.
            } else if (thisTable.getNeighbors().contains(currIP)
                    && thisTable.getDistances().get(currIP) >= TableTimerChecker.INFINITY
                    && getTimeDiff(thisTable.getTimes().get(currIP)) < TableTimerChecker.INFINITY_THRESHOLD) {
                thisTable.updateEntry(currIP, thisTable.getNeighborDistances().get(currIP) + "", currIP);
                // Update Existing entry
            } else {
                // Get current Distance
                Integer currDist = thisTable.getDistances().get(currIP);
                // Calculate new Distance from table
                Integer newDist = thisTable.getDistances().get(neighborTable.getOwner());
                if (newDist == null || newDist >= TableTimerChecker.INFINITY) {
                    // Do nothing
                    return false;
                } else {
                    newDist += neighborTable.getDistances().get(currIP);
                }
                // If the current distance is null, update with passed in distance
                // OR If the new distance is lower, set the next hop to be that distance.
                if (currDist > newDist) {
                    String nexthop = neighborTable.getOwner();
                    thisTable.updateEntry(currIP, (newDist + ""), nexthop);
                    change = true;
                } // If the next hop to my IP address is higher, but through the same person, update table to reflect that
                else if (thisTable.getNextHops().get(currIP).equals(neighborTable.getOwner()) && newDist >= currDist) {
                    String nexthop = neighborTable.getOwner();
                    thisTable.updateEntry(currIP, (newDist + ""), nexthop);
                    change = true;
                    if (thisTable.getDistances().get(currIP) >= TableTimerChecker.INFINITY) {
                        thisTable.updateEntry(currIP, (newDist + ""), "inf");
                    }

                }

            } // Finish Main loop

        }
        // Return
        return change;
    }

    /**
     * Given time, return time difference from current time.
     *
     * @param time old time
     * @return time difference
     */
    public long getTimeDiff(Long time) {
        return System.currentTimeMillis() - time;
    }

}
