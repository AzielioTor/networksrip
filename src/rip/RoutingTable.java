package rip;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * This class handles all the routingtable information for a routing table
 * @author Aziel Shaw
 * @version October 23, 2018
 */
public class RoutingTable implements Serializable {
    // Owners information
    private String ownerIP;
    private String ownerSubnet;
    // Owners neighbors information
    private ArrayList<String> direct_ips;
    private HashMap<String, Integer> direct_distances;
    // Routing table information
    //     Store all IPS known in hashmap, use each ip as KEY for VALUE.
    private ArrayList<String> ips;
    //     Store an IPs subnet with the IP address is the key
    private HashMap<String, String> subnets;
    private HashMap<String, Integer> distances;
    private HashMap<String, String> nexthops;
    // Add hashmap for time since last heard from router.
    private HashMap<String, Long> times;
    // split horizon with poisoned reverse
    
    /**
     * Constructs an empty routingtable
     */
    public RoutingTable() {
        this("Unknown", "Unknown", new ArrayList<String>(), 
                new HashMap<String, String>(), 
                new HashMap<String, Integer>());
    }
    
    /**
     * Constructs a routing table with an initial set of values
     * @param owner The IP of the owner
     * @param ownerSN The Subnet of the owner
     * @param ips The initial set of IPs the owner of the table knows
     * @param subnets The initial set of subnets the owner of this table knows
     * @param distances  The initial set of distnces the owner of this tbl knows
     */
    public RoutingTable(String owner, String ownerSN,
            ArrayList<String> ips, 
            HashMap<String, String> subnets, 
            HashMap<String, Integer> distances) {
        // Assign initial values
        this.ownerIP = owner;
        this.ownerSubnet = ownerSN;
        this.ips = ips;
        this.direct_ips = new ArrayList<>(ips);
        this.subnets = subnets;
        this.distances = distances;
        this.direct_distances = new HashMap<>(distances);
        this.times = new HashMap<>();
        this.nexthops = new HashMap<>();
        
        // Add times and next hops
        Iterator<String> itr = this.direct_ips.iterator();
        long currtime = System.currentTimeMillis();
        while(itr.hasNext()) {
            String curr = itr.next();
            times.put(curr, currtime);
            nexthops.put(curr, curr);
        }
        
        this.nexthops.put(owner, "0.0.0.0");
    }
    
    /**
     * Creates a copy of a routing table
     * @param origin The original routing table of which to copy
     */
    public RoutingTable(RoutingTable origin) {
        // Copy values
        this.ownerIP = origin.getOwner();
        this.ips = new ArrayList<>(origin.getIpsFromTable());
        this.direct_ips = new ArrayList<>(origin.getNeighbors());
        this.subnets = new HashMap<>(origin.getSubNets());
        this.distances = new HashMap<>(origin.getDistances());
        this.direct_distances = new HashMap<>(origin.getNeighborDistances());
        this.times = new HashMap<>(origin.getTimes());
        this.nexthops = new HashMap<>(origin.getNextHops());
    }
    
    /**
     * Returns the IP address of the owner of the table
     * @return The IP address
     */
    public String getOwner() {
        return this.ownerIP;
    }
    
    /**
     * Returns the neighbors of the owner of the table
     * @return The neighbors
     */
    public ArrayList<String> getNeighbors() {
        return this.direct_ips;
    }
    
    /**
     * Returns the last heard times of neighbors from the owner of the table
     * @return The times
     */
    public HashMap<String, Long> getTimes() {
        return times;
    }
    
    /**
     * Returns the distances of the neighbors of the owner of this table
     * @return The distances
     */
    public HashMap<String, Integer> getDistances() {
        return this.distances;
    }
    
    /**
     * Returns the IPs addresses of the IPs the owner of the table knows
     * @return The IP addresses
     */
    public ArrayList<String> getIpsFromTable() {
        return this.ips;
    }
    
    /**
     * Returns the subnets of the IPs the owner of the table knows
     * @return The subnets
     */
    public HashMap<String, String> getSubNets() {
        return this.subnets;
    }
    
    /**
     * Returns the next hops of the owner of the table
     * @return The next hops
     */
    public HashMap<String, String> getNextHops() {
        return this.nexthops;
    }
    
    /**
     * Returns the neighbors distances of the owner of the table
     * @return The neighbors distances
     */
    public HashMap<String, Integer> getNeighborDistances() {
        return this.direct_distances;
    }
    
    /**
     * Adds an entry into the table
     * @param ip The ip to add
     * @param subnet The subnet to add
     * @param distance The distance to add
     * @param nexthop The next hop to go to
     * @return If the addition was successful
     */
    public boolean addEntry(String ip, String subnet, String distance, String nexthop) {
        boolean success = true;
        if(this.ips.contains(ip))
            success = false;
        if(success) {
            this.subnets.put(ip, subnet);
            this.distances.put(ip, Integer.parseInt(distance));
            this.times.put(ip, System.currentTimeMillis());
            this.ips.add(ip);
            this.nexthops.put(ip, nexthop);
        }
        return success;
    }
    
    /**
     * Remove an entry from the t able
     * @param ip The IP to remove
     * @return If the removal was successful
     */
    public boolean removeEntry(String ip) {
        boolean success = true;
        if(!this.ips.contains(ip))
            success = false;
        if(success) {
            this.subnets.remove(ip);
            this.distances.remove(ip);
            this.times.remove(ip);
            this.ips.remove(ip);
            this.nexthops.remove(ip);
        }
        return success;
    }
    
    /**
     * Update an entries information
     * @param ip The up to change
     * @param distance The new distance
     * @param nexthop The new nexthop
     * @return If the change was successful
     */
    public boolean updateEntry(String ip, String distance, String nexthop) {
        boolean success = true;
        if(!this.ips.contains(ip))
            success = false;
        if(success) {
            this.distances.put(ip, Integer.parseInt(distance));
            this.nexthops.put(ip, nexthop);
        }
        return success;
    }
    
    /**
     * Update the distance of an entry
     * @param ip The IP whos distance to change
     * @param distance The new distance
     */
    public void updateDistance(String ip, Integer distance) {
        this.distances.put(ip, distance);
    }
    
    /**
     * Returns a String representation of this RoutingTable
     * @return The String representation of the RoutingTable
     */
    @Override
    public String toString() {
        // Table header
        String toReturn = "\n";
        toReturn += "/************************************************************\\\n";
        toReturn += "|   Destination   |   Subnet Mask   | Cost |    Next  Hop    |\n";
        toReturn += "|------------------------------------------------------------|\n";
        // Iterate over table
        Iterator<String> i = this.ips.iterator();
        while(i.hasNext()) {
            String ip = i.next();
            // destination = 15 characters long
            String dest = ip;
            while(dest.length() < 15)
                dest += " ";
            // subnet mask = 15 characters long
            String subnetmask = subnets.get(ip);
            while(subnetmask.length() < 15)
                subnetmask += " ";
            // cost = 4 characters long
            String cost = "";
            if(distances.get(ip) != null && distances.get(ip) < TableTimerChecker.INFINITY)
                cost = distances.get(ip).toString();
            else
                cost = "inf";
            while(cost.length() < 4)
                cost += " ";
            // next hop = 15 characters long
            String nexthop = nexthops.get(ip);
            while(nexthop.length() < 15)
                nexthop += " ";
            // Add results to string
            toReturn += "| " + dest + " | " + subnetmask + " | " + cost + " | " + nexthop + " |\n";
        }
        toReturn += "\\************************************************************/\n";
        return toReturn;
    }
    
    /**
     * Prints the table
     */
    public void printTable() {
        System.out.println(this.toString());
    }
    
    /**
     * Prints the neighbors
     */
    public void printNeighbors() {
        String result = "";
        result += "\n/*********************\\\n";
        result += "|      Neighbors      |\n";
        result += "|*********************|\n";
        for (int i = 0; i < this.direct_ips.size(); i++) {
            String currip = this.direct_ips.get(i);
            while(currip.length() < 15) 
                currip += " ";
            result += "| " + (i+1) + ":  " + currip + " |\n";
        }
        result += "\\*********************/";
        System.out.println(result);
    }
}
