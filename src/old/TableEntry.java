/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package old;

import java.net.Inet4Address;

/**
 *
 * @author azieliotor
 */
public class TableEntry {
    private Inet4Address routerIP;
    private Integer cost;
    private Inet4Address nextHop;
    
    public TableEntry(Inet4Address routerIP, Integer cost, Inet4Address nextHop) {
        this.routerIP = routerIP;
        this.cost = cost;
        this.nextHop = nextHop;
    }
    
    private Inet4Address getRouterIP() {
        return this.routerIP;
    }
    
    private Integer getCost() {
        return this.cost;
    }
    
    private Inet4Address getNextHope() {
        return this.nextHop;
    }
    
    private void setCost(Integer cost) {
        this.cost = cost;
    }
    
    private void setNextHop(Inet4Address nextHop) {
        this.nextHop = nextHop;
    }
    
}
