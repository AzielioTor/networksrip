/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rip;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author azieliotor
 */
public class SingleTableSenderThread extends Thread {
    // Socket variables
    private DatagramSocket socket;
    private final int recieverport;
    // Buffer variables
    private byte[] buffer;
    // Address to send datagram to
    private String addr;
    // Reference to the owning Router Object
    private Router r;
    // Message we're sending the router (as of right now a string)
    private String message;
    // Get Random for time differences
    private Random random;
    
    public SingleTableSenderThread(Router r, String message, String addr, 
            int recieverport) throws SocketException {
        this.r = r;
        socket = new DatagramSocket();
        this.message = message;
        this.recieverport = recieverport;
        this.addr = addr;
        this.random = new Random();
    }
    
    
    @Override
    public void run() {
        // Set up Message
        ByteArrayOutputStream byteOutStream = null;
        ObjectOutputStream outStream = null;
        try {
            byteOutStream = new ByteArrayOutputStream();
            outStream = new ObjectOutputStream(byteOutStream);
            outStream.writeObject(r.getTable());
            outStream.flush();
            buffer = byteOutStream.toByteArray();
        } catch(IOException ioe) {
            System.err.println(ioe.toString());
            ioe.printStackTrace();
        } finally {
            try {
                if(outStream != null) {
                    outStream.close();
                }
                if(byteOutStream != null) {
                    byteOutStream.close();
                }
            } catch(IOException ioe) {
                System.err.println(ioe.toString());
                ioe.printStackTrace();
            }
        }
        // Get address Set
        InetAddress address = null;
        try {
            address = InetAddress.getByName(addr);
        } catch (UnknownHostException ex) {
            Logger.getLogger(SingleTableSenderThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Set up packet
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address, this.recieverport);
        try {
//            System.out.println("Sending Packet: "  + address + ":" + this.recieverport + "  |  " + message);
            socket.send(packet);
        } catch (IOException ex) {
            Logger.getLogger(SingleTableSenderThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
