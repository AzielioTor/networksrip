/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rip;

import java.net.DatagramSocket;
import java.net.SocketException;

/**
 *
 * @author azieliotor
 */
public class TriggeredUpdateThread extends Thread {
    // Socket variables
    private DatagramSocket socket;
    // Leeprunning thread
    public boolean keeprunning = true;
    // Owner variable
    private Router router;
    
    public TriggeredUpdateThread(Router r) throws SocketException {
        this.router = r;
        socket = new DatagramSocket();
    }
    
    public void killThread() {
        this.keeprunning = false;
    }
    
    @Override
    public void run() {
        while(keeprunning) {
            
        }
    }
}
